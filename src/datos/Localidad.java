package datos;

public class Localidad {
	String nombre;
	String provincia;
	Integer habitantes;
	double latitud;
	double longitud;

	public Localidad(String nom, String prov, Integer hab, double lat, double lon) {
		this.nombre=nom;
		this.provincia=prov;
		this.habitantes=hab;
		this.latitud=lat;
		this.longitud=lon;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public double getLatitud() {
		return latitud;
	}

	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}

	public double getLongitud() {
		return longitud;
	}

	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}

}
