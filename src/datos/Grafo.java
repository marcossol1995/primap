package datos;

import java.util.ArrayList;

import negocio.LogicaGrafos;

public class Grafo {
	private static int n;
	private static double[][] matriz;
	private static ArrayList<String>listaVertices;
	private static ArrayList<Localidad>localidades;
	private static double[][]matrizCaminoMinimo;

	public Grafo(){
		
	}
	
	public static void inicializarGrafo(Grafo grafo){
		grafo.n=localidades.size();
		listaVertices=new ArrayList<String>();
		for (int i = 0; i < localidades.size(); i++) {
			listaVertices.add(localidades.get(i).getNombre());
		}
		matriz = new double[grafo.n][grafo.n];
		for (int i = 0; i < grafo.n; i++) {
			for (int j = 0; j < grafo.n; j++) {
				matriz[i][j] = LogicaGrafos.calcularCostoArista(localidades.get(i),localidades.get(j));
			}
		}
	}

	public int getN() {
		return n;
	}

	public void aumentarN() {
		this.n+=1;
	}

	public ArrayList<Localidad> getLocalidades() {
		return localidades;
	}

	public void setLocalidades(ArrayList<Localidad> localidades) {
		this.localidades = localidades;
	}

	public ArrayList<String> getListaVertices() {
		return listaVertices;
	}

	public double[][] getMatriz() {
		return matriz;
	}
	public static double[][] getMatrizCaminoMinimo() {
		return matrizCaminoMinimo;
	}

	public static void setMatrizCaminoMinimo(double[][] matrizCaminoMinimo) {
		Grafo.matrizCaminoMinimo = matrizCaminoMinimo;
	}

	public static void setListaVertices(ArrayList<String> listaVertices) {
		Grafo.listaVertices = listaVertices;
	}
	
	
	
}
