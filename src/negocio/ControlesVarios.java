package negocio;

import java.util.ArrayList;

public class ControlesVarios {

	public static boolean controlarCoordInput(String text) {
		if(text.equals(""))
			return false;
		if (!((text.charAt(0) >= '0' && text.charAt(0) <= '9') || text.charAt(0) == '-'))
			return false;
		if (text.charAt(text.length() - 1) == '.' || text.charAt(text.length() - 1)=='-' || !(text.charAt(text.length() - 1)>='0' && text.charAt(text.length() - 1)<='9'))
			return false;
		if(text.charAt(0)=='-' && text.charAt(1)=='.')
			return false;
		for (int i = 1; i < text.length() - 1; i++) {
			if (text.charAt(i) == '.') {
				if (!(text.charAt(i + 1) >= '0' && text.charAt(i + 1) <= '9'))
					return false;
			}
			if(!((text.charAt(i)>='0' && text.charAt(i)<='9')|| text.charAt(i)=='.'))
				return false;
		}
		return true;

	}
	
	public static boolean controlarHabInput(String text){
		if(text.equals(""))
			return false;
		for (int i = 0; i < text.length(); i++) {
			if(!(text.charAt(i)>='0' && text.charAt(i)<='9'))
				return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
		
	}
	
	
	
	

}
