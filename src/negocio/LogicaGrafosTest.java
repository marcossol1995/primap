package negocio;

import static org.junit.Assert.*;

import org.junit.Test;

import datos.Grafo;
import datos.Localidad;

public class LogicaGrafosTest {

	@Test 
	public void testAgregarLocalidad() {
		Grafo g= new Grafo();
		Localidad l=new Localidad("Villa de mayo", "Buenos Aires", 9578, -34.5088157, -58.6794491);
		LogicaGrafos.agregarLocalidad(g, l);
	}

	@Test
	public void testExisteLocalidad() {
		Grafo g= new Grafo();
		Localidad l=new Localidad("Villa de mayo", "Buenos Aires", 9578, -34.5088157, -58.6794491);
		Localidad x=new Localidad("Don torcuato", "Buenos Aires", 9578, -30.508348157, -58.67944591);
		LogicaGrafos.agregarLocalidad(g, l);
		assertTrue(LogicaGrafos.existeLocalidad(g.getLocalidades(),l ));
		assertFalse(LogicaGrafos.existeLocalidad(g.getLocalidades(),x ));
	}

	@Test
	public void testDistanciaCoord() {
		double distancia=LogicaGrafos.distanciaCoord(0, 0, 0, 0);
		assertEquals(0,distancia,1);
	}

	@Test
	public void testCalcularCostoArista() {
		Localidad a=new Localidad("Villa de mayo", "Buenos Aires", 9578, -34.5088157, -58.6794491);
		Localidad b=new Localidad("Don torcuato", "Buenos Aires", 9578, -30.508348157, -58.67944591);
		Localidad c=new Localidad("Don torcuato", "Salta", 9578, -30.508348157, -58.67944591);
		assertEquals(46483.16949879235,LogicaGrafos.calcularCostoArista(a, b),1);
		assertEquals(46483.16949879235+1000,LogicaGrafos.calcularCostoArista(a, c),1);
	}

	@Test
	public void testAlgPrim() {
		Grafo g=new Grafo();
		Localidad villademayo=new Localidad("villademayo", "buenosaires", 15, -34.5088157, -58.6794491);
		Localidad torcuato=new Localidad("torcuato", "buenosaires", 15, -34.4938049, -58.627271500000006);
		Localidad polvorines=new Localidad("polvorines", "buenosaires", 15, -34.4996258, -58.69144640000002);
		Localidad tortuguitas=new Localidad("tortuguitas", "buenosaires", 15, -34.47509000000001, -58.75374599999998);
		LogicaGrafos.agregarLocalidad(g, villademayo);
		LogicaGrafos.agregarLocalidad(g, torcuato);
		LogicaGrafos.agregarLocalidad(g, polvorines);
		LogicaGrafos.agregarLocalidad(g, tortuguitas);
		Grafo.inicializarGrafo(g);
		LogicaGrafos.AlgPrim(g, g.getMatriz());
	}

	@Test
	public void testTodosMarcados() {
		boolean[]vertice = new boolean[2];
		vertice[0]=true;
		assertFalse(LogicaGrafos.todosMarcados(vertice));
		vertice[1]=true;
		assertTrue(LogicaGrafos.todosMarcados(vertice));
	}

	@Test
	public void testCostoTotal() {
		Grafo g=new Grafo();
		Localidad villademayo=new Localidad("villademayo", "buenosaires", 15, -34.5088157, -58.6794491);
		Localidad torcuato=new Localidad("torcuato", "buenosaires", 15, -34.4938049, -58.627271500000006);
		Localidad polvorines=new Localidad("polvorines", "buenosaires", 15, -34.4996258, -58.69144640000002);
		Localidad tortuguitas=new Localidad("tortuguitas", "buenosaires", 15, -34.47509000000001, -58.75374599999998);
		LogicaGrafos.agregarLocalidad(g, villademayo);
		LogicaGrafos.agregarLocalidad(g, torcuato);
		LogicaGrafos.agregarLocalidad(g, polvorines);
		LogicaGrafos.agregarLocalidad(g, tortuguitas);
		Grafo.inicializarGrafo(g);
		LogicaGrafos.costoTotal(g, g.getMatrizCaminoMinimo());
	}

	@Test
	public void testMostrarCostoAristas() {
		Grafo g=new Grafo();
		Localidad villademayo=new Localidad("villademayo", "buenosaires", 15, -34.5088157, -58.6794491);
		Localidad torcuato=new Localidad("torcuato", "buenosaires", 15, -34.4938049, -58.627271500000006);
		Localidad polvorines=new Localidad("polvorines", "buenosaires", 15, -34.4996258, -58.69144640000002);
		Localidad tortuguitas=new Localidad("tortuguitas", "buenosaires", 15, -34.47509000000001, -58.75374599999998);
		LogicaGrafos.agregarLocalidad(g, villademayo);
		LogicaGrafos.agregarLocalidad(g, torcuato);
		LogicaGrafos.agregarLocalidad(g, polvorines);
		LogicaGrafos.agregarLocalidad(g, tortuguitas);
		Grafo.inicializarGrafo(g);
		LogicaGrafos.mostrarCostoAristas(g, g.getMatrizCaminoMinimo());
		
	}

}
