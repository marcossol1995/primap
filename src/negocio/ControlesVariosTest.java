package negocio;

import static org.junit.Assert.*;

import org.junit.Test;

public class ControlesVariosTest {

	@Test
	public void testControlarCoordInput() {
		assertFalse(ControlesVarios.controlarCoordInput(""));
		assertFalse(ControlesVarios.controlarCoordInput("a"));
		assertFalse(ControlesVarios.controlarCoordInput("."));
		assertFalse(ControlesVarios.controlarCoordInput("-"));
		assertTrue(ControlesVarios.controlarCoordInput("-15.84"));
	}

	@Test
	public void testControlarHabInput() {
		assertFalse(ControlesVarios.controlarHabInput("+15"));
		assertFalse(ControlesVarios.controlarHabInput("-"));
		assertFalse(ControlesVarios.controlarHabInput("a"));
		assertFalse(ControlesVarios.controlarHabInput(""));
		assertTrue(ControlesVarios.controlarHabInput("158"));
	}

}
