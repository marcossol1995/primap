package negocio;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import datos.Grafo;
import datos.Localidad;

public class LogicaGrafos {
	
	public static void agregarLocalidad(Grafo g,Localidad nuevaLocalidad) { 
		if(g.getLocalidades()==null){
			ArrayList<Localidad>primerLocalidad=new ArrayList<>();
			primerLocalidad.add(nuevaLocalidad);
			g.setLocalidades(primerLocalidad);
			g.aumentarN();
			return;
		}else{
			if(!(existeLocalidad(g.getLocalidades(),nuevaLocalidad))){
		
					g.getLocalidades().add(nuevaLocalidad);
					g.aumentarN();
					
			}
		}
		
	}
	
	public static boolean existeLocalidad(ArrayList<Localidad> lista,Localidad a){
		if(lista==null){
			return false;
		}
		for (int i = 0; i < lista.size(); i++) {
			if(lista.get(i).getNombre().equals(a.getNombre()) )
				return true;
		}
		return false;
	}

	public static double distanciaCoord(double lat1, double lng1, double lat2, double lng2) {  
        double radioTierra = 6371;
        double dLat = Math.toRadians(lat2 - lat1);  
        double dLng = Math.toRadians(lng2 - lng1);  
        double sindLat = Math.sin(dLat / 2);  
        double sindLng = Math.sin(dLng / 2);  
        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)  
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));  
        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));  
        double distancia = radioTierra * va2;  
   
        return distancia;  
    }  
	
	public static double calcularCostoArista(Localidad a,Localidad b){
		double distancia=distanciaCoord(a.getLatitud(),a.getLongitud(),b.getLatitud(),b.getLongitud());
		double costo=distancia*100;
		if(!(a.getProvincia().equals(b.getProvincia()))){
			costo+=1000;
		}
		if(distancia>=200){
			costo+=2000;
		}
		return costo;
	}
	
	public static double[][] AlgPrim(Grafo g,double[][] m) {
		boolean[] marcados = new boolean[g.getN()];
		String vertice = g.getListaVertices().get(0);
		return AlgPrim(g,m, marcados, vertice, new double[g.getN()][g.getN()]);
	}
	
	private static double[][] AlgPrim(Grafo g,double[][] m, boolean[] marcados, String vertice, double[][] Final) {
		marcados[g.getListaVertices().indexOf(vertice)]=true;
		double aux=-1;
		if(!todosMarcados(marcados)){
			for (int i = 0; i < marcados.length; i++){
				if(marcados[i]){
					 for (int j = 0; j < m.length; j++){
						 if (m[i][j] != 0){
							 if (!marcados[j]){
								 if (aux == -1){
									 aux = m[i][j];
								 }else{
	                                 aux = Math.min(aux, m[i][j]);
								 }
							 }
						 }
					 }
				}
			}
			for (int i = 0; i < marcados.length; i++) {
				 if (marcados[i]){
					 for (int j = 0; j < m.length; j++) {
						 if (m[i][j] == aux) {
							 if (!marcados[j]) {
								 Final[i][j] = aux;
								 Final[j][i] = aux;
								 return AlgPrim(g,m, marcados, g.getListaVertices().get(j), Final);
							 }
						 }
					 }
				 }
			}
		}
		g.setMatrizCaminoMinimo(Final);
		return Final;
		
		 
	 }
	 
	public static boolean todosMarcados(boolean[] vertice) {
	        for (boolean b : vertice) {
	            if (!b) {
	                return b;
	            }
	        }
	        return true;
	    }
	
	public static double costoTotal(Grafo g,double[][] matrizFinal){
		boolean[] marcados = new boolean[g.getN()];
		double total=0;
		for (int i = 0; i < marcados.length; i++) {
				for (int j = 0; j < marcados.length; j++) {
					if(marcados[j]==false){
						total+=matrizFinal[i][j];
						
					}
				}marcados[i]=true;
		}
		return total;
	}
	
	public static String mostrarCostoAristas(Grafo g,double[][] matrizFinal){
		boolean[] marcados = new boolean[g.getN()];
		String caminos="";
		for (int i = 0; i < marcados.length; i++) {
			for (int j = 0; j < marcados.length; j++) {
				if(marcados[j]==false){
					if(matrizFinal[i][j]!=0.0){
						caminos+=" "+g.getListaVertices().get(i)+" --- "+g.getListaVertices().get(j)+" = $"+matrizFinal[i][j]+"\n";
					}
				}
			}marcados[i]=true;
		}
		return caminos;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
	