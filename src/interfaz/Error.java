package interfaz;

import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.JTextArea;
import java.awt.BorderLayout;
import java.awt.Color;

public class Error extends JDialog {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Error dialog = new Error();
					dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					dialog.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the dialog.
	 */
	public Error() {
		setTitle("Error");
		setBounds(100, 100, 396, 206);
		getContentPane().setLayout(null);
		
		JTextArea txtrSiSeEncuentra = new JTextArea();
		txtrSiSeEncuentra.setBackground(Color.LIGHT_GRAY);
		txtrSiSeEncuentra.setText("Si se encuentra viendo este mensaje es porque hubo un problema \r\ncuando se intent\u00F3 a\u00F1adir la nueva localidad, tenga en cuenta:\r\n\r\n-Localidades con mismo nombre y provincia no ser\u00E1n agregadas\r\n-Las coordenadas solo pueden introducirse en decimales\r\n-Para la cantidad de habitantes se deben introducir enteros\r\n-No se puede dejar ningun espacio en blanco");
		txtrSiSeEncuentra.setEditable(false);
		txtrSiSeEncuentra.setBounds(0, 0, 514, 237);
		getContentPane().add(txtrSiSeEncuentra);

	}
}
