package interfaz;

import java.awt.EventQueue;

import javax.swing.JFrame;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;

import datos.Grafo;
import datos.Localidad;
import javax.swing.JTextField;
import javax.swing.UIManager;

import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.border.BevelBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class Principal {

	private JFrame frmPrimap;
	private JMapViewer miMapa;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal window = new Principal();
					window.frmPrimap.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Principal() {
		try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		} catch (Exception e) {
			System.out.println("error setting native LAF: " + e);
		}
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		Grafo grafo = new Grafo();
		frmPrimap = new JFrame();
		frmPrimap.setTitle("Primap");
		frmPrimap.setBounds(100, 100, 972, 549);
		frmPrimap.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		miMapa = new JMapViewer();
		frmPrimap.setContentPane(miMapa);
		miMapa.setZoomContolsVisible(false);
		miMapa.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel.setBackground(Color.GRAY);
		panel.setBounds(31, 21, 719, 112);
		miMapa.add(panel);
		panel.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(10, 11, 61, 14);
		panel.add(lblNombre);
		
		textField = new JTextField();
		textField.setBounds(94, 8, 86, 20);
		panel.add(textField);
		textField.setColumns(10);
		
		JLabel lblProvincia = new JLabel("Provincia:");
		lblProvincia.setBounds(10, 45, 74, 14);
		panel.add(lblProvincia);
		
		textField_1 = new JTextField();
		textField_1.setBounds(94, 39, 86, 20);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblHabitantes = new JLabel("Habitantes:");
		lblHabitantes.setBounds(10, 79, 74, 14);
		panel.add(lblHabitantes);
		
		textField_2 = new JTextField();
		textField_2.setBounds(94, 73, 86, 20);
		panel.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblLatitud = new JLabel("Latitud:");
		lblLatitud.setBounds(209, 11, 46, 14);
		panel.add(lblLatitud);
		
		JLabel lblLongitud = new JLabel("Longitud:");
		lblLongitud.setBounds(209, 45, 61, 14);
		panel.add(lblLongitud);
		
		textField_3 = new JTextField();
		textField_3.setBounds(268, 8, 86, 20);
		panel.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(268, 42, 86, 20);
		panel.add(textField_4);
		textField_4.setColumns(10);
		
		JButton btnVerMasDetalles = new JButton("Ver en detalle");
		btnVerMasDetalles.setEnabled(false);
		
		JButton enlazar = new JButton("Enlazar");
		enlazar.setEnabled(false);
		
		JButton calcular = new JButton("Calcular costo");
		calcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				datos.Grafo.inicializarGrafo(grafo);
				textField_5.setText(Double.toString(negocio.LogicaGrafos.costoTotal(grafo, negocio.LogicaGrafos.AlgPrim(grafo, grafo.getMatriz()))));
				btnVerMasDetalles.setEnabled(true);
				enlazar.setEnabled(true);
				
				
			}
		});
		calcular.setEnabled(false);
		calcular.setBounds(437, 41, 142, 23);
		panel.add(calcular);
		
		JButton btnAgregar = new JButton("Agregar localidad");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(negocio.ControlesVarios.controlarCoordInput(textField_3.getText()) && negocio.ControlesVarios.controlarCoordInput(textField_4.getText())){
					if(!(textField.getText().equals("")) && !(textField_1.getText().equals("")) && negocio.ControlesVarios.controlarHabInput(textField_2.getText())){
						Localidad nuevaLocalidad=new Localidad(textField.getText()+", "+textField_1.getText(),textField_1.getText() , Integer.parseInt(textField_2.getText()),Double.parseDouble(textField_3.getText()) ,Double.parseDouble(textField_4.getText()));
						if(negocio.LogicaGrafos.existeLocalidad(grafo.getLocalidades(), nuevaLocalidad)){
							Error error=new Error();
							error.setVisible(true);
						}else{
							negocio.LogicaGrafos.agregarLocalidad(grafo, nuevaLocalidad);
							MapMarker marker=new MapMarkerDot(grafo.getLocalidades().get(grafo.getN()-1).getLatitud(), grafo.getLocalidades().get(grafo.getN()-1).getLongitud());
							miMapa.addMapMarker(marker);
						}
					}
					if(grafo.getN()>=1){
						calcular.setEnabled(true);
					}
				}
				if(textField.getText().equals("") || textField_1.getText().equals("") || !(negocio.ControlesVarios.controlarHabInput(textField_2.getText()))){	
					Error error=new Error();
					error.setVisible(true);
				}
			}
		});
		btnAgregar.setBounds(209, 72, 145, 23);
		panel.add(btnAgregar);
		
		calcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnAgregar.setEnabled(false);
			}
		});
		
		JLabel lblCosto = new JLabel("Costo final:");
		lblCosto.setBounds(361, 11, 115, 14);
		panel.add(lblCosto);
		
		textField_5 = new JTextField();
		textField_5.setEditable(false);
		textField_5.setBounds(437, 8, 142, 20);
		panel.add(textField_5);
		textField_5.setColumns(10);
		
		
		btnVerMasDetalles.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Detalles detalles=new Detalles(grafo);
				detalles.setVisible(true);
				

			}
		});
		btnVerMasDetalles.setBounds(437, 75, 142, 23);
		panel.add(btnVerMasDetalles);
		
		
		ArrayList<ArrayList<Coordinate>> listaCoord=new ArrayList<>();
		enlazar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				for (int i = 0; i < grafo.getMatrizCaminoMinimo().length; i++) {
					for (int j = 0; j < grafo.getMatrizCaminoMinimo().length; j++) {
						if(grafo.getMatrizCaminoMinimo()[i][j]!=0.0){
							ArrayList<Coordinate> coord=new ArrayList<>();
							coord.add(new Coordinate(grafo.getLocalidades().get(i).getLatitud(),grafo.getLocalidades().get(i).getLongitud()));
							coord.add(new Coordinate(grafo.getLocalidades().get(j).getLatitud(),grafo.getLocalidades().get(j).getLongitud()));
							coord.add(new Coordinate(grafo.getLocalidades().get(i).getLatitud(),grafo.getLocalidades().get(i).getLongitud()));
							listaCoord.add(coord);
						}
					}
				}
				for (int i = 0; i < listaCoord.size(); i++) {
					MapPolygon polygon = new MapPolygonImpl(listaCoord.get(i));
					miMapa.addMapPolygon(polygon);
					}
				}
				
				
			
		});
		enlazar.setBounds(589, 41, 120, 23);
		panel.add(enlazar);
		
		JButton btnPreciosBase = new JButton("Precios base");
		btnPreciosBase.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PreciosBase precios=new PreciosBase();
				precios.setVisible(true);
			}
		});
		btnPreciosBase.setBounds(589, 7, 120, 23);
		panel.add(btnPreciosBase);
		
		
		
	}
}
