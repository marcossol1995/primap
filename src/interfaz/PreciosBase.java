package interfaz;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import java.awt.Color;

public class PreciosBase extends JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			PreciosBase dialog = new PreciosBase();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public PreciosBase() {
		setTitle("Precios base");
		setBounds(100, 100, 479, 172);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JTextArea txtrCostoPorKm = new JTextArea();
			txtrCostoPorKm.setBackground(Color.LIGHT_GRAY);
			txtrCostoPorKm.setText("Costo por KM : 1KM = $100\r\n\r\nSi el enlace involucra localidades de diferentes provincias = KM*$100 +$1000\r\n\r\nSi el enlace es mayor a 200KM = KM*$100 + $2000");
			txtrCostoPorKm.setEditable(false);
			txtrCostoPorKm.setBounds(0, 0, 463, 132);
			contentPanel.add(txtrCostoPorKm);
		}
		{
			JPanel buttonPane = new JPanel();
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			buttonPane.setLayout(null);
		}
	}

}
